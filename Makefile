CFLAGS=-O2 -Wall -lftd2xx -L/usr/local/lib

all: ftd2xx.dll.so libftd2xx.def

ftd2xx.o: ftd2xx_main.c ftd2xx.h ftd2xx_dll.h
	winegcc -c -D_REENTRANT -D__WINESRC__ $(CFLAGS) -o ftd2xx.o $<

ftd2xx.dll.so: ftd2xx.o ftd2xx.spec
	winegcc $(CFLAGS) -lntdll -lkernel32 -nodefaultlibs -o ftd2xx.dll $< -shared ftd2xx.spec

libftd2xx.def: ftd2xx.spec ftd2xx.dll.so
	winebuild -w --export $< --def -o libftd2xx.def

install: ftd2xx.dll.so
	install -v ftd2xx.dll.so /usr/lib/wine/
clean:
	rm ftd2xx.o ftd2xx.dll.so libftd2xx.def
