/*
 * ftd2xx.dll
 *
 * Generated from ftd2xx.dll by winedump.
 *
 * DO NOT SEND GENERATED DLLS FOR INCLUSION INTO WINE !
 * 
 */
#ifndef __WINE_FTD2XX_DLL_H
#define __WINE_FTD2XX_DLL_H

#include "windef.h"
#include "wine/debug.h"
#include "winbase.h"
#include "winnt.h"


FT_STATUS __stdcall FTD2XX_FT_Open(int deviceNumber, FT_HANDLE * pHandle);
FT_STATUS __stdcall FTD2XX_FT_Close(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_Read(FT_HANDLE ftHandle, LPVOID lpBuffer, DWORD nBufferSize, LPDWORD lpBytesReturned);
FT_STATUS __stdcall FTD2XX_FT_Write(FT_HANDLE ftHandle, LPVOID lpBuffer, DWORD nBufferSize, LPDWORD lpBytesWritten);
FT_STATUS __stdcall FTD2XX_FT_IoCtl(FT_HANDLE ftHandle, DWORD dwIoControlCode, LPVOID lpInBuf, DWORD nInBufSize, LPVOID lpOutBuf, DWORD nOutBufSize, LPDWORD lpBytesReturned, LPOVERLAPPED lpOverlapped);
FT_STATUS __stdcall FTD2XX_FT_ResetDevice(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_SetBaudRate(FT_HANDLE ftHandle, ULONG BaudRate);
FT_STATUS __stdcall FTD2XX_FT_SetDataCharacteristics(FT_HANDLE ftHandle, UCHAR WordLength, UCHAR StopBits, UCHAR Parity);
FT_STATUS __stdcall FTD2XX_FT_SetFlowControl(FT_HANDLE ftHandle, USHORT FlowControl, UCHAR XonChar, UCHAR XoffChar);
FT_STATUS __stdcall FTD2XX_FT_SetDtr(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_ClrDtr(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_SetRts(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_ClrRts(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_GetModemStatus(FT_HANDLE ftHandle, ULONG * pModemStatus);
FT_STATUS __stdcall FTD2XX_FT_SetChars(FT_HANDLE ftHandle, UCHAR EventChar, UCHAR EventCharEnabled, UCHAR ErrorChar, UCHAR ErrorCharEnabled);
FT_STATUS __stdcall FTD2XX_FT_Purge(FT_HANDLE ftHandle, ULONG Mask);
FT_STATUS __stdcall FTD2XX_FT_SetTimeouts(FT_HANDLE ftHandle, ULONG ReadTimeout, ULONG WriteTimeout);
FT_STATUS __stdcall FTD2XX_FT_GetQueueStatus(FT_HANDLE ftHandle, DWORD * dwRxBytes);
FT_STATUS __stdcall FTD2XX_FT_SetEventNotification(FT_HANDLE ftHandle, DWORD Mask, PVOID Param);
FT_STATUS __stdcall FTD2XX_FT_GetEventStatus(FT_HANDLE ftHandle, DWORD * dwEventDWord);
FT_STATUS __stdcall FTD2XX_FT_GetStatus(FT_HANDLE ftHandle, DWORD * dwRxBytes, DWORD * dwTxBytes, DWORD * dwEventDWord);
FT_STATUS __stdcall FTD2XX_FT_SetBreakOn(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_SetBreakOff(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_SetWaitMask(FT_HANDLE ftHandle, DWORD Mask);
FT_STATUS __stdcall FTD2XX_FT_WaitOnMask(FT_HANDLE ftHandle, DWORD * Mask);
FT_STATUS __stdcall FTD2XX_FT_SetDivisor(FT_HANDLE ftHandle, USHORT Divisor);
FT_STATUS __stdcall FTD2XX_FT_OpenEx(PVOID pArg1, DWORD Flags, FT_HANDLE * pHandle);
FT_STATUS __stdcall FTD2XX_FT_ListDevices(PVOID pArg1, PVOID pArg2, DWORD Flags);
FT_STATUS __stdcall FTD2XX_FT_SetLatencyTimer(FT_HANDLE ftHandle, UCHAR ucLatency);
FT_STATUS __stdcall FTD2XX_FT_GetLatencyTimer(FT_HANDLE ftHandle, PUCHAR pucLatency);
FT_STATUS __stdcall FTD2XX_FT_SetBitMode(FT_HANDLE ftHandle, UCHAR ucMask, UCHAR ucEnable);
FT_STATUS __stdcall FTD2XX_FT_GetBitMode(FT_HANDLE ftHandle, PUCHAR pucMode);
FT_STATUS __stdcall FTD2XX_FT_SetUSBParameters(FT_HANDLE ftHandle, ULONG ulInTransferSize, ULONG ulOutTransferSize);
FT_STATUS __stdcall FTD2XX_FT_EraseEE(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_ReadEE(FT_HANDLE ftHandle, DWORD dwWordOffset, LPWORD lpwValue);
FT_STATUS __stdcall FTD2XX_FT_WriteEE(FT_HANDLE ftHandle, DWORD dwWordOffset, WORD wValue);
FT_STATUS __stdcall FTD2XX_FT_EE_Program(FT_HANDLE ftHandle, PFT_PROGRAM_DATA pData);
FT_STATUS __stdcall FTD2XX_FT_EE_Read(FT_HANDLE ftHandle, PFT_PROGRAM_DATA pData);
FT_STATUS __stdcall FTD2XX_FT_EE_UARead(FT_HANDLE ftHandle, PUCHAR pucData, DWORD dwDataLen, LPDWORD lpdwBytesRead);
FT_STATUS __stdcall FTD2XX_FT_EE_UASize(FT_HANDLE ftHandle, LPDWORD lpdwSize);
FT_STATUS __stdcall FTD2XX_FT_EE_UAWrite(FT_HANDLE ftHandle, PUCHAR pucData, DWORD dwDataLen);
FT_HANDLE __stdcall FTD2XX_FT_W32_CreateFile(LPCSTR lpszName, DWORD dwAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreate, DWORD dwAttrsAndFlags, HANDLE hTemplate);
BOOL __stdcall FTD2XX_FT_W32_CloseHandle(FT_HANDLE ftHandle);
BOOL __stdcall FTD2XX_FT_W32_ReadFile(FT_HANDLE ftHandle, LPVOID lpBuffer, DWORD nBufferSize, LPDWORD lpBytesReturned, LPOVERLAPPED lpOverlapped);
BOOL __stdcall FTD2XX_FT_W32_WriteFile(FT_HANDLE ftHandle, LPVOID lpBuffer, DWORD nBufferSize, LPDWORD lpBytesWritten, LPOVERLAPPED lpOverlapped);
BOOL __stdcall FTD2XX_FT_W32_GetOverlappedResult(FT_HANDLE ftHandle, LPOVERLAPPED lpOverlapped, LPDWORD lpdwBytesTransferred, BOOL bWait);
BOOL __stdcall FTD2XX_FT_W32_ClearCommBreak(FT_HANDLE ftHandle);
BOOL __stdcall FTD2XX_FT_W32_ClearCommError(FT_HANDLE ftHandle, LPDWORD lpdwErrors, LPFTCOMSTAT lpftComstat);
BOOL __stdcall FTD2XX_FT_W32_EscapeCommFunction(FT_HANDLE ftHandle, DWORD dwFunc);
BOOL __stdcall FTD2XX_FT_W32_GetCommModemStatus(FT_HANDLE ftHandle, LPDWORD lpdwModemStatus);
BOOL __stdcall FTD2XX_FT_W32_GetCommState(FT_HANDLE ftHandle, LPFTDCB lpftDcb);
BOOL __stdcall FTD2XX_FT_W32_GetCommTimeouts(FT_HANDLE ftHandle, FTTIMEOUTS * pTimeouts);
DWORD __stdcall FTD2XX_FT_W32_GetLastError(FT_HANDLE ftHandle);
BOOL __stdcall FTD2XX_FT_W32_PurgeComm(FT_HANDLE ftHandle, DWORD dwMask);
BOOL __stdcall FTD2XX_FT_W32_SetCommBreak(FT_HANDLE ftHandle);
BOOL __stdcall FTD2XX_FT_W32_SetCommMask(FT_HANDLE ftHandle, ULONG ulEventMask);
BOOL __stdcall FTD2XX_FT_W32_SetCommState(FT_HANDLE ftHandle, LPFTDCB lpftDcb);
BOOL __stdcall FTD2XX_FT_W32_SetCommTimeouts(FT_HANDLE ftHandle, FTTIMEOUTS * pTimeouts);
BOOL __stdcall FTD2XX_FT_W32_SetupComm(FT_HANDLE ftHandle, DWORD dwReadBufferSize, DWORD dwWriteBufferSize);
BOOL __stdcall FTD2XX_FT_W32_WaitCommEvent(FT_HANDLE ftHandle, PULONG pulEvent, LPOVERLAPPED lpOverlapped);
FT_STATUS __stdcall FTD2XX_FT_GetDeviceInfo(FT_HANDLE ftHandle, FT_DEVICE * lpftDevice, LPDWORD lpdwID, PCHAR SerialNumber, PCHAR Description, LPVOID Dummy);
BOOL __stdcall FTD2XX_FT_W32_CancelIo(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_StopInTask(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_RestartInTask(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_SetResetPipeRetryCount(FT_HANDLE ftHandle, DWORD dwCount);
FT_STATUS __stdcall FTD2XX_FT_ResetPort(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_EE_ProgramEx(FT_HANDLE ftHandle, PFT_PROGRAM_DATA lpData, char * Manufacturer, char * ManufacturerId, char * Description, char * SerialNumber);
FT_STATUS __stdcall FTD2XX_FT_EE_ReadEx(FT_HANDLE ftHandle, PFT_PROGRAM_DATA lpData, char * Manufacturer, char * ManufacturerId, char * Description, char * SerialNumber);
FT_STATUS __stdcall FTD2XX_FT_CyclePort(FT_HANDLE ftHandle);
FT_STATUS __stdcall FTD2XX_FT_CreateDeviceInfoList(LPDWORD lpdwNumDevs);
FT_STATUS __stdcall FTD2XX_FT_GetDeviceInfoList(FT_DEVICE_LIST_INFO_NODE * pDest, LPDWORD lpdwNumDevs);
FT_STATUS __stdcall FTD2XX_FT_GetDeviceInfoDetail(DWORD dwIndex, LPDWORD lpdwFlags, LPDWORD lpdwType, LPDWORD lpdwID, LPDWORD lpdwLocId, LPVOID lpSerialNumber, LPVOID lpDescription, FT_HANDLE * pftHandle);
FT_STATUS __stdcall FTD2XX_FT_SetDeadmanTimeout(FT_HANDLE ftHandle, ULONG ulDeadmanTimeout);



#endif	/* __WINE_FTD2XX_DLL_H */
